module.exports = function(grunt) {
    "use strict";
    var os = require('os');
    require("load-grunt-tasks")(grunt);
    var PATHS = {
        js: [
            '*.json',
            '*.js',
            'src/**/*.json',
            'src/**/*.js'
        ]
    };
    var pkg = grunt.file.readJSON('package.json');
    var version = pkg.version;
    var short = pkg.short;
    var description = pkg.description;
    var name = pkg.title;
	var namespace = pkg.namespace;

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: ['dist', 'temp'],
        jshint: {
            js: [
                "src/**/*.js"
            ],
            options: {
                esnext: true,
                reporter: require('jshint-summary'),
                ignores: ['Gruntfile.js', 'src/**/jquery*.js', 'src/**/perfect*.js', 'src/**/lzma-d.js', 'src/**/piwik.lib.js'],
            },
            all: PATHS.js
        },
        copy: {
            main: {
                files: [
                    { expand: true, cwd: 'src/shims/chrome/', src: ['**'], dest: 'temp/' + name + '/', filter: 'isFile' },
                    { expand: true, cwd: 'src/shims/chrome/', src: ['manifest.json'], dest: 'temp/' + name + '/', filter: 'isFile' }
                ]
            }
        },
        'string-replace': {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'src/shims/chrome/',
                    src: 'manifest.json',
                    dest: 'temp/' + name
                },{
                    expand: true,
                    cwd: 'src/shims/chrome/js/',
                    src: '*.js',
                    dest: 'temp/' + name + '/js/'
                }
				],
                options: {
                    replacements: [{
                            pattern: /__name__/g,
                            replacement: name
                        },
                        {
                            pattern: /__version__/g,
                            replacement: version
                        },
                        {
                            pattern: /__short__/g,
                            replacement: short
                        },
                        {
                            pattern: /__description__/g,
                            replacement: description
                        },
						{
                            pattern: /__namespace__/g,
                            replacement: namespace
                        }
                    ]
                }
            }
        },
        zip: {
            chrome: {
                cwd: 'temp/' + name + '/',
                src: ['temp/' + name + '/**/*', ],
                dest: 'public/' + short + '_' + version + '.zip'
            }
        }
    });

    // Default task(s).
    grunt.registerTask('default', ['clean', 'jshint:all', 'copy', 'string-replace', 'zip']);
};
