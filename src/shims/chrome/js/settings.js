if (!__namespace__) var __namespace__ = {};

__namespace__.$settings = {
    options: {
        inject: {
            search: 1,
            dark: 1,
            selection: 1
        },
        search: {
            bing: 1,
            google: 0,
            yahoo: 0,
            youtube: 0
        }
    },
    setOptions: function(options) {
        __namespace__.$settings.options = options;
        chrome.storage.local.set({
            options: __namespace__.$settings.options
        }, function() {});
    }
};
chrome.storage.local.get("options", function(result) {
    try {
        if (typeof result.options == "undefined") {
            chrome.storage.local.set({
                options: __namespace__.$settings.options
            }, function() {});
        } else {
            __namespace__.$settings.options = result.options;
        }
    } catch (ex) {}
});
