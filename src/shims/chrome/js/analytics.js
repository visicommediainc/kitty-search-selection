if (!__namespace__)
    var __namespace__ = {};

__namespace__.$analytics = {
    $ga: {
        _AnalyticsCode: "UA-133826869-1",
        init: function() {},
        trackPage: function(page, customUrl) {
            var request = new XMLHttpRequest();
            var message =
                "v=1&tid=" + __namespace__.$analytics.$ga._AnalyticsCode + "&cid=" + __namespace__.$initialize.user_id + "&aip=1" +
                "&cn=" + __namespace__.$events.campaign_id + "&ds=add-on&t=pageview&dp=" + page + "&dr=" + customUrl;

            console.log("trackPage.message=" + message);
            request.open("POST", "https://www.google-analytics.com/collect", true);
            request.send(message);
        },
        trackEvent: function(category, action, label, value) {
            var request = new XMLHttpRequest();
            var message = "tid=" + __namespace__.$analytics.$ga._AnalyticsCode + "&cid=" + __namespace__.$initialize.user_id;
            message += "&cn=" + __namespace__.$initialize.campaign_id + "&t=event&ec=" + category + "&v=1";

            if (action && action !== '')
                message += "&ea=" + action;

            if (label && label !== '')
                message += "&el=" + label;

            console.log("ga.trackEvent.message=" + message);
            request.open("POST", "https://www.google-analytics.com/collect", true);
            request.send(message);
        }
    },
    $piwik: {
        url: "https://analytics.vmn.net/",
        cid: 35,
        pwTracker: {},
        getPwTracker: function(baseUrl, siteId) {
            var u = baseUrl.replace(/\/$/, "");
            var tracker = function() {
                // arguments does not implement Array common function (shift, slice....)
                // Piwik make use of these function on _paq instance thus the necessity
                // of casting arguments as Array
                (window._paq = window._paq || []).push(Array.prototype.slice.call(arguments));
            };
            tracker = Piwik.getTracker(u + '/piwik.php', siteId);
            return tracker;
        },
        init: function() {
            __namespace__.$analytics.$piwik.pwTracker = __namespace__.$analytics.$piwik.getPwTracker(__namespace__.$analytics.$piwik.url, __namespace__.$analytics.$piwik.cid);
        },
        trackPage: function(page, customUrl) {
            var url = 'https://__namespace__.net/?pk_campaign=' + __namespace__.$initialize.campaign_id;
            __namespace__.$analytics.$piwik.pwTracker.setUserId(__namespace__.$initialize.user_id);
            __namespace__.$analytics.$piwik.pwTracker.setCustomUrl(url);
            __namespace__.$analytics.$piwik.pwTracker.setDocumentTitle(page); // replace wlcome with the page name
            __namespace__.$analytics.$piwik.pwTracker.trackPageView(); // replace welcome with page name
        },
        trackEvent: function(category, action, label, value) {
            var url = 'https://__namespace__.net/?pk_campaign=' + __namespace__.$initialize.campaign_id;
            console.log("piwik.trackEvent.url=" + url);
            __namespace__.$analytics.$piwik.pwTracker.setUserId(__namespace__.$initialize.user_id);
            __namespace__.$analytics.$piwik.pwTracker.setCustomUrl(url);
            __namespace__.$analytics.$piwik.pwTracker.trackEvent(category, action, label, value);
        }
    },
    trackPage: function(page, customUrl) {
        __namespace__.$analytics.$ga.trackPage(page, customUrl);
        __namespace__.$analytics.$piwik.trackPage(page, customUrl);
    },
    trackEvent: function(category, action, label, value) {
        __namespace__.$analytics.$ga.trackEvent(category, action, label, value);
        __namespace__.$analytics.$piwik.trackEvent(category, action, label, value);
        console.log("trackEvent:" + category + "|" + action + "|" + label + "|" + value);
    }
};

document.addEventListener("DOMContentLoaded", function() {
    __namespace__.$analytics.$ga.init();
    __namespace__.$analytics.$piwik.init();
});
