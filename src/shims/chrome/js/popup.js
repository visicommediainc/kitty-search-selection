var mode = 1;
var selection = 1;

function defaultSearch(evt) {
    if (evt.keyCode === 13 || (evt.target && !evt.keyCode)) {
        doSearch();
    }
}

function doSearch(e) {
    var href = "https://searchresult.co/extensions/?pr=vmnkitty&id=bin&v=1_0&q=";// https://vmn.fastsearch.me/?sub=ksearch&q=";
    var val = document.getElementById("term").value.trim();

    if (val.length < 3) {
        return;
    }

    //alert(curTarget + val);
    chrome.tabs.create({ url: href + val, active: true });

    // default Search
    chrome.extension.sendMessage({ message: 'trackevent', action: "Kitty_Search", label: "Input_Box", value: "default" });
}

function onLoad() {
    document.getElementById("term").onkeypress = defaultSearch;
    document.getElementById("searchBtn").onclick = defaultSearch;
    document.getElementById("term").focus();
    //document.getElementById("clh").onclick = darkMode;
}
document.addEventListener("DOMContentLoaded", onLoad, false);
