__namespace__.$browser = {

    onMessage: function(request, sender, sendResponse) {
        switch (request.message) {
            case 'trackpage':
                {
                    __namespace__.$analytics.trackPage(request.title, request.page);
                }
                break;
            case 'trackevent':
                {
                    __namespace__.$analytics.trackEvent(request.action, request.action, request.label, request.value);
                }
                break;
            case 'get_options':
                {
                    sendResponse(__namespace__.$settings.options);
                }
                break;
            case 'inject':
                {
                    //sendResponse(__namespace__.$settings.options);
                    // console.log("inject:" + request.value);
                    __namespace__.$settings.options.inject.search = request.value;
                    __namespace__.$analytics.trackEvent("Kitty_Search", "Kitty_Search", 'Ckeckbox_Inject', request.value);
                }
                break;
            case 'select':
                {
                    //sendResponse(__namespace__.$settings.options);
                    // console.log("selection:" + request.value);
                    __namespace__.$settings.options.inject.selection = request.value;
                    __namespace__.$analytics.trackEvent("Kitty_Search", "Kitty_Search", 'Ckeckbox_Selection', request.value);
                }
                break;
            case 'dark-mode':
                {
                    // console.log("dark-mode:" + request.value);
                    __namespace__.$settings.options.inject.dark = request.value;
                    __namespace__.$analytics.trackEvent("Kitty_Search", "Kitty_Search", request.message, request.value);
                }
        }
    }
};
document.addEventListener("DOMContentLoaded", function() {
    chrome.extension.onMessage.addListener(__namespace__.$browser.onMessage);
});


