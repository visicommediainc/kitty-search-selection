__namespace__.$initialize = {
    hostName: 'com.mystart.one.newtab.' + chrome.runtime.id,
    campaign_id: "",
    click_id: "",
    user_id: "",
    details: "",
    config: {},
    getCampaignId: function(callback) {
        chrome.runtime.sendNativeMessage(__namespace__.$initialize.hostName, {
            endpoint: 'get-campaign-id'
        }, function(response) {
            if (chrome.runtime.lastError) {
                response = {
                    error: 'BROKER_NOT_FOUND',
                    campaign_id: null
                };
            }

            callback(response);
        });
    },
    getUserId: function(callback) {
        chrome.runtime.sendNativeMessage(__namespace__.$initialize.hostName, {
            endpoint: 'get-user-id'
        }, function(response) {
            if (chrome.runtime.lastError) {
                response = {
                    error: 'BROKER_NOT_FOUND',
                    user_id: null
                };
            }

            callback(response);
        });
    },
    getConfig: function(callback) {
        var xhr = new XMLHttpRequest();
        xhr.open("GET", chrome.extension.getURL('/config.json'), true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    __namespace__.$initialize.config = JSON.parse(xhr.responseText);
                    callback();
                }
            }
        }; // Implemented elsewhere.
        xhr.send();
    },
    guid: function() {
        var d = new Date().getTime();
        var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
        return uuid;
    },
    setUninstallPage: function() {
        // chrome.runtime.setUninstallURL(__namespace__.$events.UNINSTALL_URL + __namespace__.$initialize.campaign_id, function() {});
    },
    sendPing: function() {
        console.log("sendPing");
        var pingUrl =
            console.log("sendPing.url=" + __namespace__.$initialize.config.ping + __namespace__.$initialize.click_id + "&gclid=" + __namespace__.$initialize.campaign_id);

        var xhr = new XMLHttpRequest();
        xhr.open("GET", __namespace__.$initialize.config.ping + __namespace__.$initialize.click_id + "&gclid=" + __namespace__.$initialize.campaign_id, true);
        xhr.onreadystatechange = function() {
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    var response = JSON.parse(xhr.responseText);
                    console.log("ping response.result=" + response.result);
                }
            }
        }; // Implemented elsewhere.
        xhr.send();
    },
    $init: function() {
        console.log("broker init");
        __namespace__.$initialize.getConfig(function() {
            window.setTimeout(function() {
                __namespace__.$utils.getRemoteInfo(__namespace__.$initialize.config.api_url, function(response) {
                    try {
                        console.log("api.reponse=" + JSON.stringify(response));
                        var json = JSON.parse(response);
                        var cid = json.cid;
                        var clickid = json.clickid;
                        if ((cid === null || cid.indexOf("9950nt") === -1) && __namespace__.$initialize.details === "install") {
                            console.log("redirect to congrats page!");
                            chrome.tabs.getSelected(null, function(tab) {
                                chrome.tabs.update(tab.id, {
                                    url: __namespace__.$events.INSTALLED_URL
                                });
                            });
                        }

                        if (cid === null)
                            return;
                        __namespace__.$initialize.campaign_id = cid;
                        chrome.storage.local.set({ "campaign_id": __namespace__.$initialize.campaign_id });
                        if (clickid !== null) {
                            __namespace__.$initialize.click_id = clickid;
                            chrome.storage.local.set({ "click_id": __namespace__.$initialize.click_id });
                        }
                        __namespace__.$initialize.setUninstallPage();
                    } catch (e) {}
                });
            }, 2000);
        });
        __namespace__.$initialize.user_id = __namespace__.$initialize.guid();
        __namespace__.$initialize.campaign_id = "123";
        chrome.storage.local.get("campaign_id", function(result) {
            if (result.campaign_id)
                __namespace__.$initialize.campaign_id = result.campaign_id;
        });
        chrome.storage.local.get("user_id", function(result) {
            if (result.user_id)
                __namespace__.$initialize.user_id = result.user_id;
        });


        __namespace__.$initialize.getCampaignId(function(data) {
            if (!data.error && data.campaign_id) {
                __namespace__.$initialize.campaign_id = data.campaign_id;
                chrome.storage.local.set({ "campaign_id": __namespace__.$initialize.campaign_id });
                __namespace__.$initialize.setUninstallPage();
            }
        });

        __namespace__.$initialize.getUserId(function(data) {
            if (!data.error && data.user_id) {
                __namespace__.$initialize.user_id = data.user_id;
                chrome.storage.local.set({ "user_id": __namespace__.$initialize.user_id });
            }
        });

        chrome.runtime.onInstalled.addListener(function(details) {
            __namespace__.$initialize.details = details.reason;
            if (details.reason == "install") {
                setTimeout(function() {
                    console.log("__namespace__.$initialize.click_id=" + __namespace__.$initialize.click_id);
                    console.log("__namespace__.$initialize.glc_id=" + __namespace__.$initialize.campaign_id);
                    if (__namespace__.$initialize.click_id !== "") {
                        __namespace__.$initialize.sendPing();
                    }
                    __namespace__.$analytics.trackEvent("Runtime_Install", __namespace__.$initialize.campaign_id, " ", " ");
                }, 5000);
                __namespace__.$initialize.setUninstallPage();
            } else if (details.reason == "update") {
                __namespace__.$analytics.trackEvent("Runtime_Update", __namespace__.$initialize.campaign_id, " ", " ");
            }
        });
    }
};

document.addEventListener("DOMContentLoaded", function() {
    __namespace__.$initialize.$init();
});
