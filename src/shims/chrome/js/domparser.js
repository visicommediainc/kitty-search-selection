function addEvent(elms, type, handler) {
    for (i = 0; i < elms.length; i++) {
        el = elms[i];
        el.addEventListener(type, handler);
    }
}

function removeEvent(elms, type, handler) {
    for (i = 0; i < elms.length; i++) {
        el = elms[i];
        el.removeEventListener(type, handler);
    }
}

function setImgPos(img) {
    var form = document.forms[0];
    var submit = form.querySelectorAll("input[type=submit]")[0];
    var bounding = form.getBoundingClientRect();
    var margin = 0;
     if (submit !== undefined) {
        bounding = submit.getBoundingClientRect();
    }
    var right = bounding.left + bounding.width + 10;
    if (form.action.indexOf('.yahoo.') !== -1) {
        margin = 500;
    }
    if ((document.documentElement.clientWidth - right - margin) >= 80) {
        img.style.left = right + "px";
        img.style.top = "5px";
    } else {
        img.style.left = "10px";
        img.style.top = "50px";
    }
}

if (!__namespace__)
    var __namespace__ = {};
__namespace__.$domParser = {
    search: "",
    init: function() {
        __namespace__.$domParser.backgroundColor = document.body.style.backgroundColor;
        var currentUrl = document.location.hostname + "";
        if (currentUrl.indexOf("bing.com") == -1) {
            return;
        }
        var search = "";
        if (currentUrl.indexOf("bing.com") != -1) {
            search = "bing";
        }
        if (search === "") {
            return;
        }
        __namespace__.$domParser.search = search;
        chrome.extension.sendMessage({ message: "get_options" }, function(options) {
            __namespace__.$domParser.inject(options);
        });
    },
    injectImage: function() {
        var div = document.createElement("DIV");
        div.id = "__namespace__Div";
        firstImg = document.createElement("img");
        firstImg.id = "__namespace__FirstImg";
        firstImg.style.position = "absolute";
        firstImg.style.zIndex = "999999";
        var num = chrome.storage.local.get("imageIndex", function(idx) {
            if (typeof idx.imageIndex === 'undefined') {
                chrome.storage.local.set({ "imageIndex": "1" });
                num = 1;
            } else {
                num = idx.imageIndex + 1;
            }
            if (num > 15) {
                num = 1;
            }
            firstsearch_ico = chrome.extension.getURL('images/' + num + '.gif');
            firstImg.src = firstsearch_ico;
            div.appendChild(firstImg);
            document.body.append(div);
            setImgPos(firstImg);
            chrome.storage.local.set({ "imageIndex": num });
        });
        window.addEventListener("resize", function() {
            var firstImg = document.getElementById("__namespace__FirstImg");
            setImgPos(firstImg);
        });
    },
    inject: function(options) {
        try {
            var currentUrl = document.location.hostname + "";
            if (currentUrl.indexOf("bing.com") == -1  || options.inject.search === 0) {
                return;
            }
            search = __namespace__.$domParser.search;
            if (search === "") {
                return;
            }
            mark = "__namespace___item";
            switch (search) {
                case "bing":
                    {
                        className = ".b_algo";
                    }
                    break;
            }
            marker = document.querySelector(mark);
            if (marker) {
                return;
            }
            setTimeout(function(){
                elms = document.querySelectorAll(className);

                if (elms.length >0 && elms[0].offsetWidth > 0 && elms[0].offsetHeight > 0) {
                    //console.log("start scanning " + search);
                    //console.log("links.length=" + elms.length);
                    __namespace__.$domParser.injectImage();
                }
            }, 1000);
        } catch (e) {
            //alert(e.message);
        }
    }
};
__namespace__.$domParser.init();
